eastbots-java-code: Eastbots's Java Code
==========================================================

Introduction
------------

This is an implementation of the drive train code in Java.
Eastbots will be participating in FRC - FIRST Robotics and this is the programming team's project.

Contributors & Team Members: Ming Yang, Charles Zhang(Scapegoat), Amalan Iyengar

Flashing & Connecting to the Router
-----------------------------------

Current WPA Password: eastbots

To flash the router, open FRC Configuration Utility. Then set team number to 4795 and have everything on bridge mode.

Connect to the robot router statically (DO NOT USE DHCP) at the address (someone fill this in)

Working with the source 
-----------------------

NetBeans is used as the primary IDE for development.
In order to compile and test a seperate wpl plugin must be installed.

###Update the build.xml###
1. Right click on the Command project
2. Properties
3. Go to Java Sources Classpath
4. Add JAR/Folder
5. Select ~/sunspotfrcsdk/lib/WPILibj/classes.jar
6. Hope it works


Code Overview
-------------

Command-based programming is used.
Currently the code is organized as follows:
Everything thrown into 9 layers of folders, with Vision code seperate from src

Resources & Documentation
-------------------------

###Library Docs###

Documentation for the robot-specific classes we are using:
	
	http://www.wbrobotics.com/javadoc/edu/wpi/first/wpilibj/package-summary.html
	
Documention for cv2 is here

	http://docs.opencv.org/trunk/doc/py_tutorials/py_tutorials.html

###Build Enviroment###

Documentation for build enviroment is here:
	
	http://first.wpi.edu/Images/CMS/First/Getting_Started_with_Java_for_FRC.pdf

###General Programming Info###
Some info on command based programming here:

	http://wpilib.screenstepslive.com/s/3120/m/7952/l/105519-what-is-command-based-programming



