import cv2
import cv2.cv as cv
import numpy as np
import time
import threshold as t


def circleToMask(circle, img):
    mask = np.zeros((img.shape), np.uint8)
    cv2.circle(mask,(circle[0],circle[1]) ,circle[2],(255,255,255),-1)
    mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
    return mask


def bestCircle(circles):
    index = 0
    largest = 0
    for i in range(1,len(circles)):
        i = circles[0,i]
        #mask = circleToMask(i, hsv)
        if(i[2] > largest):
            largest = i[2]
            index = i
    return circles[0][index]


def findBall(img):
    lower_red = np.array([0,100,50])
    upper_red = np.array([15,255,255])
    lower_red2 = np.array([160,100,100])
    upper_red2 = np.array([180,255,255])
    lower_blue = np.array([90,50,0])
    upper_blue = np.array([130,255,255])
    (res, mask) = t.threshold(img, lower_blue, upper_blue, True)
    res = cv2.GaussianBlur(res, (11,11), 0)
    gray = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
    cv2.imshow("image", gray)
    cv2.waitKey(0)
    circles = cv2.HoughCircles(gray, cv.CV_HOUGH_GRADIENT, 1, 20, param1=100
                               , param2=30, minRadius=0, maxRadius=0)
    img_circles = img
    if not circles is None:
        i = bestCircle(circles)
        cv2.circle(img_circles,(i[0],i[1]),i[2],(0,255,0),2)
        # draw the center of the circle
        cv2.circle(img_circles,(i[0],i[1]),2,(0,0,255),3)
    return img_circles
    print circles


def start():
	#http://IP/mjpg/video.mjpg
	#http://IP/axis-cgi/mjpg/video.cgi?camera=1&resolution=352x288
	cap = cv2.VideoCapture('blue.avi')
	while(cap.isOpened()):
		a, frame = cap.read()
		start = time.clock()
		frame = findBall(frame)
		cv2.imshow("image", frame)
		finish =  time.clock()
		cv2.waitKey(0)
		print finish - start
		
if __name__ == '__main__':
	start()




