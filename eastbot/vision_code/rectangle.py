import sys
import cv2
import cv2.cv as cv
import numpy as np
import threshold as t


#parse arguments and return file input
def parsearg():
    if (len(sys.argv) < 2):
        print "please pass a file argument"
        return 1

    if (len(sys.argv) > 2):
        print "too many file args"
        return 1
    args = sys.argv
    #should be input file
    return args[1]


def displayIm(img):
    cv2.imshow("image", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def distance(a, b):  # a and b are 2-tuples
    return ((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2) ** 0.5


def drawRects(img, contours):
    h_array = []  # list of horizontal rect points in quadrant III & IV
    v_array = []  # vertical in quadrant I & II
    for i in contours:
        #contours[i] = cv2.approxPolyDP(i, 5, False)
        x, y, w, h = cv2.boundingRect(i)
        if getVertical(x, y, w, h):
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0),
                          thickness=3)
            v_array.append((x, y))
            v_array.append((x + w, y))
        if getHorizontal(x, y, w, h):
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255),
                          thickness=3)
            h_array.append((x, y + h))
            h_array.append((x + w, y + h))
    hotspot_distance = 25  # find a good distance for this
    hotspot_array = []  # horizontal coords of hotspot
    for a in h_array:
        for b in v_array:
            if distance(a, b) < hotspot_distance:
                hotspot_array.append(b)
    for i in set(hotspot_array):
        print "hotspot at:", i
        draw_hotspot(i, img)

    return img


def draw_hotspot(hotspot, image):
    cv2.circle(image, (hotspot[0], hotspot[1]), 5, [255, 0, 0], thickness=2)


def getVertical(x, y, w, h):
    if not (h == 0 or w == 0):
        if h/w > 2 and h/w < 9:
            return True
    return False


def getHorizontal(x, y, w, h):
    if not (h == 0 or w == 0):
        if w/h > 1 and w/h < 7:
            return True
    return False


def rectangle():
    input = parsearg()
    print input
    if input == 1:
        print "failed to open file!"
        return 1
    print "successfully opened file!"
    img = cv2.imread(input)
    lower_color = np.array([50, 100, 60])  # green
    upper_color = np.array([100, 255, 255])  
    (img, mask) = t.threshold(img, lower_color, upper_color, False)
    displayIm(mask)       
    contours = cv2.findContours(mask, cv2.RETR_TREE,
                                cv2.CHAIN_APPROX_TC89_KCOS)[0]
    drawRects(img, contours)
    cv2.drawContours(img, contours, -1, (255, 255, 255), thickness=1)
    displayIm(img)i

if __name__ == '__main__':
    rectangle()
