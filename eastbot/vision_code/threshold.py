import cv2
import numpy as np

def threshold(img, lower_bound, upper_bound, blur):
    
    #convert to hsv
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    
    #binary mask
    mask = cv2.inRange(hsv, lower_bound, upper_bound)

    if(blur):
        mask = cv2.blur(mask, (11,11))

    #magic hole fixer
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2,2), anchor=(1,1))
    output = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel,
                              iterations=5)

    #apply mask to image
    res = cv2.bitwise_and(img,img, mask=output)
    return (res, mask)

