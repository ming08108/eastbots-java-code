package edu.wpi.first.wpilibj.templates;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
    // For example to map the left and right motors, you could define the
    // following variables to use with your drivetrain subsystem.
    // public static final int leftMotor = 1;
    // public static final int rightMotor = 2;
    // PWM OUTPUTS
    public static final int leftMotorUno =             1;
    public static final int rightMotorUno =            2;
    public static final int leftMotorDue =             3;
    public static final int rightMotorDue =            4;
    public static final int leftCollector =            5;
    public static final int rightCollector =           6;
    public static final int winchMotor =               7;
    public static final int latchServo =               8;
    //DIGITAL I/O OUTPUTS
    public static final int winchEncoderA =            4;
    public static final int winchEncoderB =            6;
    public static final int winchEncoderIndex =        7;
    public static final int limitSwitch =              14;
    /**
    * 
    * loaderArm limit switches
    * 
    */
    
    
    //CONSTANTS
    public static final int atonSpeed = 160; //speed to move drive motors in PWM (0-255)
    public static final double timeOut = 1.0; //Time to move during aton in seconds!
    public static final boolean disableAton = true; //do we want to enable aton? true = disabled
    
    
    // If you are using multiple modules, make sure to define both the port
    // number and the module. For example you with a rangefinder:
    // public static final int rangefinderPort = 1;
    // public static final int rangefinderModule = 1;
}
