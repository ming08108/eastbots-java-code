package edu.wpi.first.wpilibj.templates.subsystems;

import edu.wpi.first.wpilibj.PWM;
import edu.wpi.first.wpilibj.templates.RobotMap;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.templates.subsystems.Debug;
/*
 *
 * @author charleszhang
 */
public class CollectorSubsystem extends Subsystem {
    PWM leftCollector = new PWM(RobotMap.leftCollector);
    PWM rightCollector = new PWM(RobotMap.rightCollector);
    
    //half speed right away on collectors
    public void startCollect(boolean isCollect) {
        if (isCollect) {
            Debug.print("Collect set to: Forwards ");
            leftCollector.setRaw(191);
            rightCollector.setRaw(191);
        }
        else {
            Debug.print("Collect set to: Reverse ");
            leftCollector.setRaw(63);
            rightCollector.setRaw(63);
        }
    }
    public void stopCollect() {
        leftCollector.setRaw(127);
        rightCollector.setRaw(127);
    }

    public void initDefaultCommand() {
        
    }
}
