package edu.wpi.first.wpilibj.templates.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.templates.RobotMap;
/**
 *
 * @author charliezhang
 */
public class ServoSubsystem extends Subsystem {
    
    Servo catapultServo = new Servo(RobotMap.latchServo);
    
    public void openServo() {
        catapultServo.set(0);
        Debug.print("Servo Opened ");
    }
    
    public void closeServo() {
        catapultServo.set(0.60);
        Debug.print("Servo Closed ");
    }
    
    public double getServo() {
        return catapultServo.get();
    }
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
