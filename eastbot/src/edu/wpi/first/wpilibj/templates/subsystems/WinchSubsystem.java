package edu.wpi.first.wpilibj.templates.subsystems;

import edu.wpi.first.wpilibj.PWM;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.templates.RobotMap;
/**
 *
 * @author charleszhang
 */
public class WinchSubsystem extends Subsystem {
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
    PWM winchMotor = new PWM(RobotMap.winchMotor);
    
    public void upWinch() {
        winchMotor.setRaw(254);
        Debug.print("Upwinch ");
    }
    
    public void stopWinch() {
        winchMotor.setRaw(127);
    }
    
    public void downWinch() {
        winchMotor.setRaw(1);
        Debug.print("Downwinch ");
    }
    
    public void getRaw() {
        winchMotor.getRaw();
    }

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
}
