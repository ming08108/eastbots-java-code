package edu.wpi.first.wpilibj.templates.subsystems;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.templates.RobotMap;

/**
 *
 * @author charleszhang
 */
public class EncoderSubsystem extends Subsystem {
    
    Encoder amsEncoder = new Encoder(RobotMap.winchEncoderA, 
            RobotMap.winchEncoderB, false);
    
    //last argument reverses direction
    
    public void start() {
        amsEncoder.start();
    }
    
    public void stop() {
        amsEncoder.stop();
    }
    
    public void reset() {
        amsEncoder.reset();
    }
    
    public void setDistancePerPulse(double distancePerPulse) {
        Debug.print("Setting distance per pulse to:" + distancePerPulse);
        amsEncoder.setDistancePerPulse(distancePerPulse);
    }
    
    public int getCount() {
        return amsEncoder.get();
    }  
    
    public boolean getDirection() {
        return amsEncoder.getDirection();
    }
    
    public double getDistance() {
        return amsEncoder.getDistance();
    }
    
    public void initDefaultCommand() {
        
    }
}
