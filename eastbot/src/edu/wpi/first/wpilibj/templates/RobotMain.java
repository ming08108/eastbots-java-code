/*----------------------------------------------------------------------------*/
/* Copyright (c) FIRST 2008. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package edu.wpi.first.wpilibj.templates;


import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.PWM;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.templates.commands.CommandBase;
import edu.wpi.first.wpilibj.templates.commands.AutonomousCommand;
import edu.wpi.first.wpilibj.templates.commands.DriveTrainCommand;
import edu.wpi.first.wpilibj.templates.commands.ManualCommand;
import edu.wpi.first.wpilibj.templates.commands.CatapultCommand;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class RobotMain extends IterativeRobot {

    Command autonomousCommand;
    Command driveTrainCommand;
    Command manualCommand;
    Command catapultCommand;

    /**
     * This function is run when the robot is first started up and should be
     * used for any initialization code.
     */
    public void robotInit() {
        // instantiate the command used for the autonomous period
        //autonomousCommand = new ExampleCommand();
        
        // Initialize all subsystems
        CommandBase.init();
        autonomousCommand = new AutonomousCommand();
    }

    public void autonomousInit() {
        // schedule the autonomous command (example)
        autonomousCommand.start();
    }

    /**
     * This function is called periodically during autonomous
     */
    public void autonomousPeriodic() {
        Scheduler.getInstance().run();
    }

    public void teleopInit() {
	// This makes sure that the autonomous stops running when
        // teleop starts running. If you want the autonomous to 
        // continue until interrupted by another command, remove
        // this line or comment it out.
        autonomousCommand.cancel();
        
        driveTrainCommand = new DriveTrainCommand();
        manualCommand = new ManualCommand();
        catapultCommand = new CatapultCommand();
    }

    /**
     * This function is called periodically during operator control
     */
    public void teleopPeriodic() {
        Scheduler.getInstance().run();
    }
    
    
    public void testInit() {
        LiveWindow.addActuator("DRIVE", "leftMotorUno", new PWM(RobotMap.leftMotorUno));
        LiveWindow.addActuator("DRIVE", "leftMotorDue", new PWM(RobotMap.leftMotorDue));
        
        LiveWindow.addActuator("DRIVE", "rightMotorUno", new PWM(RobotMap.rightMotorUno));
        LiveWindow.addActuator("DRIVE", "rightMotorDue", new PWM(RobotMap.rightMotorDue));
        
        
        
        LiveWindow.addActuator("COLLECTOR", "leftCollector", new PWM(RobotMap.leftCollector));
        LiveWindow.addActuator("COLLECTOR", "rightCollector", new PWM(RobotMap.rightCollector));
        
        LiveWindow.addActuator("CATAPULT", "winchMotor", new PWM(RobotMap.winchMotor));
        LiveWindow.addActuator("CATAPULT", "latchServo", new PWM(RobotMap.latchServo));
        
        
        //sensors
        LiveWindow.addSensor("CATAPULT", "winchEncoder", new Encoder(RobotMap.winchEncoderA, RobotMap.winchEncoderB, false));
        LiveWindow.addSensor("CATAPULT", "limitSwitch", new DigitalInput(RobotMap.limitSwitch));
        
    }
    
    
    public void testPeriodic() {
        LiveWindow.run();
    }
}
