package edu.wpi.first.wpilibj.templates.commands;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.templates.OI;
import edu.wpi.first.wpilibj.templates.subsystems.Debug;





/**
 * NOT FINISHED DO NOT USE
 * @author ming
 */
public class TwistDriveCommand extends CommandBase{		
    
    Joystick right;
    Joystick left;

    public TwistDriveCommand() {
        requires(driveTrainSubsystem);
        left = oi.leftJoy;
        right = oi.rightJoy;
    }
    protected void initialize() {
        Debug.print("TwistDrive Command Init");
    }

    protected void execute() {
        //TODO DRIVETRAIN CODE HERE
        double leftY = left.getY();
        double leftTwist = left.getTwist();
        	
        int leftV = (int) (toPWM(leftY) - toPWM(leftTwist));
        
        int rightV = (int) (toPWM(leftY) + toPWM(leftTwist));
        
        
        //set drive motor (TANK)
        //deadzone handled by DriveTrainSubsystem!
         driveTrainSubsystem.setLeftMotor(leftV);
         driveTrainSubsystem.setLeftMotor(rightV);
    }

    protected boolean isFinished() {
        return false; // I am never finished!!!!
    }

    protected void end() {
        
    }

    protected void interrupted() {
        
    }
    
    //converts -1 - 1 to 0-255
    public int toPWM(double joyY) {
        return ((int) (127 * joyY) - 127); //JoyY should be -1 - 1
    }
    
    public double toZeroOne(double v) {
        return (v+1)/2;
    }
    
}
