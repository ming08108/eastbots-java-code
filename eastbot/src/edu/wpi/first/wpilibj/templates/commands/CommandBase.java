package edu.wpi.first.wpilibj.templates.commands;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.templates.OI;
import edu.wpi.first.wpilibj.templates.subsystems.DriveTrainSubsystem;
import edu.wpi.first.wpilibj.templates.subsystems.EncoderSubsystem;
import edu.wpi.first.wpilibj.templates.subsystems.CollectorSubsystem;
import edu.wpi.first.wpilibj.templates.subsystems.WinchSubsystem;
import edu.wpi.first.wpilibj.templates.subsystems.LimitSubsystem;
import edu.wpi.first.wpilibj.templates.subsystems.ServoSubsystem;

/**
 * The base for all commands. All atomic commands should subclass CommandBase.
 * CommandBase stores creates and stores each control system. To access a
 * subsystem elsewhere in your code in your code use CommandBase.exampleSubsystem
 * @author Author
 */
public abstract class CommandBase extends Command {

    public static OI oi;
    // Create a single static instance of all of your subsystems
    public static DriveTrainSubsystem driveTrainSubsystem =     new DriveTrainSubsystem();
    public static EncoderSubsystem encoderSubsystem =           new EncoderSubsystem();
    public static CollectorSubsystem collectorSubsystem =       new CollectorSubsystem();
    public static WinchSubsystem winchSubsystem =               new WinchSubsystem();
    public static LimitSubsystem limitSubsystem =               new LimitSubsystem();
    public static ServoSubsystem servoSubsystem =               new ServoSubsystem();
    
    public static void init() {
        // This MUST be here. If the OI creates Commands (which it very likely
        // will), constructing it during the construction of CommandBase (from
        // which commands extend), subsystems are not guaranteed to be
        // yet. Thus, their requires() statements may grab null pointers. Bad
        // news. Don't move it.
        oi = new OI();

        // Show what command your subsystem is running on the SmartDashboard
        
    }

    public CommandBase(String name) {
        super(name);
    }

    public CommandBase() {
        super();
    }
}
