package edu.wpi.first.wpilibj.templates.commands;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.templates.subsystems.Debug;


/**
 *
 * @author ming
 */
public class DriveTrainCommand extends CommandBase{
    
    Joystick left = oi.leftJoy;
    Joystick right = oi.rightJoy;

    public DriveTrainCommand() {
        requires(driveTrainSubsystem);
    }
    
    protected void initialize() {
        Debug.print("Drive Train Command Init");
    }
    
    protected void execute() {
        double leftY = left.getY(); //unused
        double rightY = right.getY(); //unused
        //set drive motor (TANK)
        //deadzone handled by DriveTrainSubsystem!
        
        if (toPWM(leftY) > 0)
            driveTrainSubsystem.setLeftMotor(toPWM(leftY));

        if (toPWM(rightY) > 0)
            driveTrainSubsystem.setRightMotor(toPWM(rightY));		
        // Print out the desired raw speed
        else if (leftY <= 0)
            driveTrainSubsystem.setLeftMotor(1);

        else if (rightY <= 0)
            driveTrainSubsystem.setLeftMotor(1);
        
        if (right.getRawButton(4))
            driveTrainSubsystem.setLeftMotor(140);
        
        if (right.getRawButton(5))
            driveTrainSubsystem.setRightMotor(140); 
    }

    protected boolean isFinished() {
        return false; // I am never finished!!!!
    }

    protected void end() {
        
    }

    protected void interrupted() {
        
    }
    
    //converts range of -1 - 1 to 0 - 255
    public int toPWM(double joyY) {
        return ((int) (127 * joyY + 127)); //JoyY should be -1 - 1
    }
    
}
