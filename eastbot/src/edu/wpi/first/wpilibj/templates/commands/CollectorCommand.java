package edu.wpi.first.wpilibj.templates.commands;

import edu.wpi.first.wpilibj.templates.subsystems.Debug;
import edu.wpi.first.wpilibj.Joystick;
/**
 *
 * @author charleszhang
 */
public class CollectorCommand extends CommandBase {
    
    private boolean isCollect; // True when collect
                               // False for reverse
    
    public CollectorCommand(boolean direction) {
        isCollect = direction;       
        requires(collectorSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        Debug.print("Collecting Init");
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        collectorSubsystem.startCollect(isCollect);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
        collectorSubsystem.stopCollect();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        collectorSubsystem.stopCollect();
    }
}
