package edu.wpi.first.wpilibj.templates.commands;

import edu.wpi.first.wpilibj.templates.subsystems.Debug;

/**
 *
 * @author charleszhang
 */
public class CatapultCommand extends CommandBase {
    
    protected boolean manualUp = false;
    protected boolean manualDown = false;
    protected boolean autoUpMode = false;
    protected boolean autoDownMode = false;
    protected static long startTime = System.currentTimeMillis();
    
    public CatapultCommand() {
        requires(winchSubsystem);
        requires(limitSubsystem);        
        requires(servoSubsystem);
        requires(encoderSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        Debug.print("Catapult command init");
        encoderSubsystem.start();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        Debug.print("\nNORMAL MODE\n");
        Debug.print("Encoder Count: " + encoderSubsystem.getCount() + " ");
                
        if (oi.leftJoy.getRawButton(1)) { //trigger
            servoSubsystem.openServo();
            Debug.print("Servo Opened!");
        }
        // true = winch above bottom false = all the way down
        else if (oi.leftJoy.getRawButton(2) && limitSubsystem.getLimit()) {
            winchSubsystem.downWinch();
            startTime = System.currentTimeMillis();
            autoDownMode = true;
        }
        
        else if (oi.leftJoy.getRawButton(3)) {
            winchSubsystem.upWinch();
            autoUpMode = true;
        }
        
        else if (System.currentTimeMillis() == startTime + 10000) {
            winchSubsystem.stopWinch();
            Debug.print("JAMMED\n");
        }
        
        else if (!limitSubsystem.getLimit() && autoDownMode) {
            winchSubsystem.stopWinch();
            servoSubsystem.closeServo();
            autoDownMode = false;
        }
        
        else if (autoUpMode == true && encoderSubsystem.getCount() < 0) {
            winchSubsystem.stopWinch();
            encoderSubsystem.reset();
        }
        
        else if (oi.leftJoy.getRawButton(6)) {
            encoderSubsystem.reset();
        }
        
        else if (oi.rightJoy.getRawButton(8)) {
            servoSubsystem.closeServo();
        }
        
        else if (oi.rightJoy.getRawButton(9)) {
            servoSubsystem.openServo();
        }
        
        else if (oi.rightJoy.getRawButton(6)) {
            winchSubsystem.upWinch();
            manualUp = true;
        }
        
        else if (!oi.rightJoy.getRawButton(6) && manualUp) {
            winchSubsystem.stopWinch();
            encoderSubsystem.reset();
            manualUp = false;
        }
        
        else if (oi.rightJoy.getRawButton(7)) {
            winchSubsystem.downWinch();
            manualDown = true;
        }
        
        else if (!oi.rightJoy.getRawButton(7) && manualDown) {
            winchSubsystem.stopWinch();
            encoderSubsystem.reset();
            manualDown = false;
        }
    }
    
    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() { 
        return false; // don't get GC'd into a wall
    }

    // Called once after isFinished returns true
    protected void end() {
        winchSubsystem.stopWinch();
        encoderSubsystem.stop();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        
    }
}
