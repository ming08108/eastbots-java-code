package edu.wpi.first.wpilibj.templates.commands;

import edu.wpi.first.wpilibj.templates.subsystems.Debug;
import edu.wpi.first.wpilibj.Joystick;
/**
 *
 * @author charleszhang
 */
public class ManualCommand extends CommandBase {

    public ManualCommand() {
        requires(winchSubsystem);
        requires(limitSubsystem);
        requires(servoSubsystem);
        requires(encoderSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
        Debug.print("Manual Command init\n");
        encoderSubsystem.start();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        Debug.print("YOU ARE IN MANUAL MODE\n");
        Debug.print("YOU ARE IN MANUAL MODE\n");
        Debug.print("Limit: " + limitSubsystem.getLimit() + " ");
        Debug.print("Encoder Count: " + encoderSubsystem.getCount() + " ");
        
        if (oi.leftJoy.getRawButton(6)) {
            encoderSubsystem.reset();
        }
        
        else if (oi.leftJoy.getRawButton(5)) {
            servoSubsystem.openServo();
        }
        
        else if (oi.leftJoy.getRawButton(4)) {
            servoSubsystem.closeServo();
        }
        
        else if (oi.leftJoy.getRawButton(3)) {
            winchSubsystem.upWinch();
        }
        
        else if (oi.leftJoy.getRawButton(2)) {
            winchSubsystem.downWinch();
        }
        
        else {
            winchSubsystem.stopWinch();
        }
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false; //manual commands should never finish
    }

    // Called once after isFinished returns true
    protected void end() {
        encoderSubsystem.stop();
        winchSubsystem.stopWinch();
        servoSubsystem.closeServo(); //stop everything if it does somehow
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        winchSubsystem.stopWinch();
        encoderSubsystem.stop();
    }
}
