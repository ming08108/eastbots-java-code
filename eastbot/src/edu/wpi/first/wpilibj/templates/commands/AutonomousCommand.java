/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wpi.first.wpilibj.templates.commands;

import edu.wpi.first.wpilibj.templates.RobotMap;


/**
 *
 * @author ming
 */
public class AutonomousCommand extends CommandBase{

    public AutonomousCommand() {
        requires(driveTrainSubsystem);
    }
    
    protected void initialize() {
        setTimeout(RobotMap.timeOut); //time to move in aton
    }

    protected void execute() {
        if(!RobotMap.disableAton) {
            driveTrainSubsystem.setLeftMotor(RobotMap.atonSpeed);
            driveTrainSubsystem.setRightMotor(RobotMap.atonSpeed);
        }
    }

    protected boolean isFinished() {
        return isTimedOut() || RobotMap.disableAton;
    }

    protected void end() {
        //STOP THE MOTORS
        driveTrainSubsystem.setLeftMotor(128);
        driveTrainSubsystem.setRightMotor(128);
    }

    protected void interrupted() {
        //STOP THE MOTORS
        driveTrainSubsystem.setLeftMotor(128);
        driveTrainSubsystem.setRightMotor(128);
    }
    
}
